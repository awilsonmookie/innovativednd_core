﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using InnovativeDnd.Core.Repository.DatabaseObjects;
using InnovativeDnd.Core.Repository.Enums;
using InnovativeDnd.Core.Repository.Interfaces;

namespace InnovativeDnd.Core.Repository
{
    public class EmployeeRepository: IRepository<EmployeeDTO>
    {
        private readonly string _connectionString;

        public EmployeeRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["AdventureWorksRepository"].ToString();
        }

        public IEnumerable<EmployeeDTO> SelectAll()
        {
            var employeeDtoList = new List<EmployeeDTO>();

            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                using (var sqlCommand = new SqlCommand() {CommandType = CommandType.StoredProcedure, CommandText = Utilities.GetDescription(StoredProcedures.GetEmployees), Connection = sqlConnection })
                {
                    using (var sqlReader = sqlCommand.ExecuteReader())
                    {
                        if (sqlReader.HasRows)
                        {
                            PopulateList(sqlReader, employeeDtoList);
                        }
                    }
                }
            }
            return employeeDtoList;
        }

        public IEnumerable<EmployeeDTO> SelectByFilter(Dictionary<string, string> selectCriteria, StoredProcedures storedProcedure)
        {
            throw new NotImplementedException();
        }

        public EmployeeDTO SelectById(int id)
        {
            var employeeDto = new EmployeeDTO();

            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                using (var sqlCommand = new SqlCommand() { CommandType = CommandType.StoredProcedure, CommandText = Utilities.GetDescription(StoredProcedures.GetEmployeeById), Connection = sqlConnection })
                {
                    using (var sqlReader = sqlCommand.ExecuteReader())
                    {
                        if (sqlReader.HasRows)
                        {
                            
                        }
                    }
                }
            }

            return employeeDto;
        }

        public void Insert(EmployeeDTO obj)
        {
            throw new NotImplementedException();
        }

        public void Update(EmployeeDTO obj)
        {
            throw new NotImplementedException();
        }

        public void UpdateList(IEnumerable<EmployeeDTO> updateItems)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void PopulateList(SqlDataReader dataReader, List<EmployeeDTO> listDtos)
        {
            while (dataReader.Read())
            {
                var employeeDto = new EmployeeDTO();
                var employeeDeptHistoryDto = new EmployeeDeptHistoryDTO();

                employeeDto.AdditionalContactInfo = dataReader["AdditionalContactInfo"].ToString();
                employeeDto.AddressLine1 = dataReader["AddressLine1"].ToString();
                employeeDto.AddressLine2 = dataReader["AddressLine2"].ToString();
                employeeDto.BusinessEntityId = Utilities.CheckForDbNull_Value(dataReader["BusinessEntityId"]);
                employeeDto.City = dataReader["City"].ToString();
                employeeDto.CountryRegionName = dataReader["CountryRegionName"].ToString();
                employeeDto.EmailAddress = dataReader["EmailAddress"].ToString();
                employeeDto.EmailPromotion = Utilities.CheckForDbNull_Value(dataReader["EmailPromotion"]);
                employeeDto.FirstName = dataReader["FirstName"].ToString();
                employeeDto.JobTitle = dataReader["JobTitle"].ToString();
                employeeDto.LastName = dataReader["LastName"].ToString();
                employeeDto.MiddleName = dataReader["MiddleName"].ToString();
                employeeDto.Phone = dataReader["Phone"].ToString();
                employeeDto.PostalCode = dataReader["PostalCode"].ToString();
                employeeDto.StateProvinceName = dataReader["StateProvinceName"].ToString();
                employeeDto.Suffix = dataReader["Suffix"].ToString();
                employeeDto.Title = dataReader["Title"].ToString();

                employeeDeptHistoryDto.BusinessEntityId = Utilities.CheckForDbNull_Value(dataReader["BusinessEntityId"]);
                employeeDeptHistoryDto.DepartmentName = dataReader["DepartmentName"].ToString();
                employeeDeptHistoryDto.EndDate = Utilities.CheckForDbNull_Time(dataReader["EndDate"]);
                employeeDeptHistoryDto.GroupName = dataReader["GroupName"].ToString();
                employeeDeptHistoryDto.StartDate = Utilities.CheckForDbNull_Time(dataReader["StartDate"]);
                employeeDto.DepartmentHistory.Add(employeeDeptHistoryDto);

                var existingDto = listDtos.Single(x => x.BusinessEntityId == employeeDto.BusinessEntityId);
                if (existingDto == null)
                {
                    listDtos.Add(employeeDto);
                }
                else
                {
                    existingDto.DepartmentHistory.Add(employeeDto.DepartmentHistory[0]);
                }
            }
        }

        public EmployeeDTO Populate(SqlDataReader dataReader)
        {
            var employeeDto = new EmployeeDTO();

            if (dataReader.HasRows)
            {
                while (dataReader.Read())
                {
                    var employeeDeptHistoryDto = new EmployeeDeptHistoryDTO();

                    // it iis possible for the dataReader to have multiple rows for one employee (meaning they been in more than one department). So in order to not take up time going through repopulating the 
                    // employee informaition again....check the business entity Id. If a pass has been made through at least once...then it will be beyond the diefault initialize value of an integer.
                    if (employeeDto.BusinessEntityId == 0)
                    {
                        employeeDto.AdditionalContactInfo = dataReader["AdditionalContactInfo"].ToString();
                        employeeDto.AddressLine1 = dataReader["AddressLine1"].ToString();
                        employeeDto.AddressLine2 = dataReader["AddressLine2"].ToString();
                        employeeDto.BusinessEntityId = Utilities.CheckForDbNull_Value(dataReader["BusinessEntityId"]);
                        employeeDto.City = dataReader["City"].ToString();
                        employeeDto.CountryRegionName = dataReader["CountryRegionName"].ToString();
                        employeeDto.EmailAddress = dataReader["EmailAddress"].ToString();
                        employeeDto.EmailPromotion = Utilities.CheckForDbNull_Value(dataReader["EmailPromotion"]);
                        employeeDto.FirstName = dataReader["FirstName"].ToString();
                        employeeDto.JobTitle = dataReader["JobTitle"].ToString();
                        employeeDto.LastName = dataReader["LastName"].ToString();
                        employeeDto.MiddleName = dataReader["MiddleName"].ToString();
                        employeeDto.Phone = dataReader["Phone"].ToString();
                        employeeDto.PostalCode = dataReader["PostalCode"].ToString();
                        employeeDto.StateProvinceName = dataReader["StateProvinceName"].ToString();
                        employeeDto.Suffix = dataReader["Suffix"].ToString();
                        employeeDto.Title = dataReader["Title"].ToString();                        
                    }

                    employeeDeptHistoryDto.BusinessEntityId = Utilities.CheckForDbNull_Value(dataReader["BusinessEntityId"]);
                    employeeDeptHistoryDto.DepartmentName = dataReader["DepartmentName"].ToString();
                    employeeDeptHistoryDto.EndDate = Utilities.CheckForDbNull_Time(dataReader["EndDate"]);
                    employeeDeptHistoryDto.GroupName = dataReader["GroupName"].ToString();
                    employeeDeptHistoryDto.StartDate = Utilities.CheckForDbNull_Time(dataReader["StartDate"]);
                    employeeDto.DepartmentHistory.Add(employeeDeptHistoryDto);
                }
            }
            return employeeDto;
        }
    }
}
