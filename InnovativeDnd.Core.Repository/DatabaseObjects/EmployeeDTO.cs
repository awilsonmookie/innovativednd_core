﻿using System;
using System.Collections.Generic;
using InnovativeDnd.Core.Repository.Interfaces;

namespace InnovativeDnd.Core.Repository.DatabaseObjects
{
    public class EmployeeDTO : IDbEntity
    {
        public EmployeeDTO() { DepartmentHistory = new List<EmployeeDeptHistoryDTO>();}

        public Int32 BusinessEntityId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string JobTitle { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public int EmailPromotion { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateProvinceName { get; set; }
        public string PostalCode { get; set; }
        public string CountryRegionName { get; set; }
        public string AdditionalContactInfo { get; set; }
        public List<EmployeeDeptHistoryDTO> DepartmentHistory { get; set; }
    }
}
