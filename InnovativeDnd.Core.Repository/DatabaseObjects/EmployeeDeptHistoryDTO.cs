﻿using System;
using InnovativeDnd.Core.Repository.Interfaces;

namespace InnovativeDnd.Core.Repository.DatabaseObjects
{
    public class EmployeeDeptHistoryDTO : IDbEntity
    {
        public Int32 BusinessEntityId { get; set; }
        public string DepartmentName { get; set; }
        public string GroupName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
