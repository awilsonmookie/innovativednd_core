﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace InnovativeDnd.Core.Repository.Enums
{
    public enum RepositoryViews
    {
        [Display(Name="HumanResources.vEmployee")]
        VEmployee,

        [Display(Name="HummanResources.vEmployeeDepartment")]
        VEmployeeDepartment,

        [Display(Name="HumanResources.vEmployeeDepartmentHistory")]
        VEmployeeDepartmentHistory
    }
}
