﻿using System.ComponentModel;

namespace InnovativeDnd.Core.Repository.Enums
{
    public enum StoredProcedures
    {
        [Description("dbo.uspGetEmployees")]
        GetEmployees,

        [Description("dbo.uspGetEmployeesById")]
        GetEmployeeById

    }
}
