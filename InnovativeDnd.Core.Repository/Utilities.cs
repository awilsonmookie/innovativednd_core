﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InnovativeDnd.Core.Repository
{
    public static class Utilities
    {
        public static string GetDescription(Enum currentEnum)
        {
            System.Reflection.FieldInfo oFieldInfo = currentEnum.GetType().GetField(currentEnum.ToString());
            var attributes = (DescriptionAttribute[])oFieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : currentEnum.ToString();
        }

        public static string CheckForDbNull_String(object checkObject)
        {
            if (checkObject is System.DBNull)
            {
                return string.Empty;
            }
            else
            {
                return checkObject.ToString();
            }
        }

        public static DateTime CheckForDbNull_Time(object checkObject)
        {
            if (checkObject is System.DBNull)
            {
                return DateTime.Parse("01/01/1753");
            }
            else
            {
                return (DateTime)checkObject;
            }
        }

        public static Int32 CheckForDbNull_Value(object checkObject)
        {
            if (checkObject is System.DBNull)
            {
                return 0;
            }
            else
            {
                return (Int32)checkObject;
            }
        }
    }
}
