﻿using System;


namespace InnovativeDnd.Core.Repository.Interfaces
{
    public interface IDbEntity
    {
        Int32 BusinessEntityId { get; set; }
    }
}