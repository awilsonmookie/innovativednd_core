﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace InnovativeDnd.Core.Repository.Interfaces
{
    public interface IRepository<T> where T : IDbEntity
    {
        IEnumerable<T> SelectAll();
        IEnumerable<T> SelectByFilter(Dictionary<string, string> selectCriteria, Enums.StoredProcedures storedProcedure);
        T SelectById(int id);

        void Insert(T obj);
        void Update(T obj);
        void UpdateList(IEnumerable<T> updateItems);
        void Delete(int id);
        void Save();

        void PopulateList(SqlDataReader dataReader, List<T> listDtos);
        T Populate(SqlDataReader dataReader);
    }
}
